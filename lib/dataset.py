"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import pandas as pd
import os 
import numpy as np
from .annotationTransformation import *

class Dataset:
    def __init__(self,path,maxNr = 0):
        self.path = path
        self.tabletSegments = []
        if maxNr > 0:
            self.maxNr = maxNr
        else:
            self.maxNr = len(os.listdir(path))
        self.readJsonFiles()

    def readJsonFiles(self):
        for file in os.listdir(self.path)[:self.maxNr]:
            if '.json' in file:
                df = pd.read_json(f'{self.path}/{file}')
                #the id is in front of png
                annotationsId = file.split(".png")[0]
                #contains annotations? 
                containsWedgeAnnotations = Dataset.containsAnnotationType(df,"Wedgetype")
                containsTransliterations = Dataset.containsAnnotationType(df,"Transliteration")
                #extract annotations to numpy arrays
                wedgeAnnotations = []
                if containsWedgeAnnotations:
                    wedgeAnnotations = Dataset.getAnnotations(df,"Wedgetype")
                
                if containsTransliterations:
                    characterAnnotations = Dataset.getAnnotations(df,"Transliteration")

                #only use Annotations with wedges or transliterations
                if (containsWedgeAnnotations or containsTransliterations):
                    
                    tabletEntry = {"id": annotationsId,
                                    "containsWedgeAnnotations": containsWedgeAnnotations,
                                    "containsTransliterations": containsTransliterations,
                                    "rawData": df,
                                    "characters": characterAnnotations,
                                    "wedges": wedgeAnnotations}
                    self.tabletSegments.append(tabletEntry)
            

    def createDirIfNotExists(path):
        #if the directory doesen't exist then create the directory
        if not os.path.exists(path):
            os.makedirs( path )
    
    def containsAnnotationType(dataFrame,type):
        #search in each annotations entry for the type/purpose
        for key in dataFrame.keys():
            #sometimes no "Purpose" is given (type)
            try:
                for annotationMeta in dataFrame[key].body:
                    #print(annotationMeta)
                    annotationPurpose = annotationMeta['purpose']
                    if annotationPurpose == type:
                        return True 
            except:
                pass
        return False
            
    def deleteSegmentWithoutImage(self,imageDirectory):
        imageFiles = os.listdir(imageDirectory)
        newTabletSegments = []
        for i, seg in enumerate(self.tabletSegments):
            segId = seg["id"]
            if f'{segId}.jpg' in imageFiles:
                #do not remove in iteration, this leads to problems with the iterator
                newTabletSegments.append(seg)
                #self.tabletSegments.remove(seg)
     
        self.tabletSegments = newTabletSegments

    def getAnnotations(dataFrame,type):
        annotations = []
        for key in dataFrame.keys():
            #polygons are used for annotations
            if dataFrame[key].target["selector"]["type"] == "SvgSelector":
                #is annotation of the given type?
                for annotationMeta in dataFrame[key].body:
                    #only extract the polygon if the annotation has the correct type
                    try: 
                        if(annotationMeta['purpose'] == type):
                            #org entry 
                            #<svg><polygon points="717.244873046875,407.1689758300781 604.6828002929688,425.2344970703125 615.800048828125,604.5 729.7517700195312,593.3828125"></polygon></svg>
                            polygonPoints = []
                            polygonStr = dataFrame[key].target["selector"]["value"].split('"')[1]
                            pointsAsString = polygonStr.split(" ")
                            for point in pointsAsString:
                                coordinates = point.split(",")
                                coordsAsInt = [int(round(float(coordinates[0]))),int(round(float(coordinates[1])))]
                                polygonPoints.append(coordsAsInt)
                            
                            annotations.append(np.array([polygonPoints]))
                            break
                    except Exception as e:
                        print(dataFrame[key].target["selector"])
                        print(e)
                        continue
            #bounding boxes are used for annotations and convert to 4 points as well
            # use the same structure like polygons 
            if dataFrame[key].target["selector"]["type"] == "FragmentSelector":
                #is annotation of the given type?
                for annotationMeta in dataFrame[key].body:
                    #only extract the bounding box information if the annotation has the correct type
                    try: 
                        if(annotationMeta['purpose'] == type):
                            #org entry 
                            #"xywh=pixel:538.0361938476562,231.3452911376953,167.65130615234375,156.95021057128906"
                            boundingBoxStr = dataFrame[key].target["selector"]["value"].split(':')[1]
                            xywhAsString = boundingBoxStr.split(",")
                            polygonPoints = Dataset.convertXYWHStringsTo4points(xywhAsString)                  
                            annotations.append(np.array([polygonPoints]))
                            break
                    except Exception as e:
                        print(dataFrame[key].target["selector"])
                        print(e)
                        continue
        return annotations
            
    def getSegmentById(self,id):
        for seg in self.tabletSegments:
            if seg['id'] == id:
                return seg
        return False

    def convertXYWHStringsTo4points(xywh):
        x,y,w,h = xywh
        x = float(x)
        y = float(y)
        w = float(w)
        h = float(h)
    
        p1 = [int(round(x)),int(round(y))]
        p2 = [int(round(x+w)),int(round(y))]
        p3 = [int(round(x+w)),int(round(y+h))]
        p4 = [int(round(x)),int(round(y+h))]

        return [p1,p2,p3,p4]
    
    #this method searchs for the segment by index in tabletSegments
    #the given transformation (trafo matrix) is executed on the annoations (polygon points and bbox points)
    #the polygons and bboxes will be overwritten 
    #the new dataframe is saved as result
    def transfAndSaveSegJsonByIdx(self,idx,trafoMatrix,resultPath):
        trafoMatrix = np.concatenate((np.array(trafoMatrix),np.array([[0,0,1]])),axis=0)
        #for the annotation we have to use back projection
        trafoMatrix = np.linalg.inv(trafoMatrix)
        seg = self.tabletSegments[idx]
        for key in seg["rawData"].keys():
            #polygons are used for annotations
            if seg["rawData"][key].target["selector"]["type"] == "SvgSelector":
                #only extract the polygon if the annotation has the correct type
                try: 
                    #org entry 
                    #<svg><polygon points="717.244873046875,407.1689758300781 604.6828002929688,425.2344970703125 615.800048828125,604.5 729.7517700195312,593.3828125"></polygon></svg>
                    polygonPoints = []
                    polygonStr = seg["rawData"][key].target["selector"]["value"].split('"')[1]
                    pointsAsString = polygonStr.split(" ")
                    for point in pointsAsString:
                        coordinates = point.split(",")
                        coordsAsInt = [int(round(float(coordinates[0]))),int(round(float(coordinates[1])))]
                        polygonPoints.append(coordsAsInt)
                    #print(polygonPoints)
                    polygonPoints = transformPolygonPoints(np.array(polygonPoints),trafoMatrix)
   
                except Exception as e:
                    print(seg["rawData"][key].target["selector"])
                    print(e)
                    continue
            #bounding boxes are used for annotations
            if seg["rawData"][key].target["selector"]["type"] == "FragmentSelector":
                #only extract the bounding box information if the annotation has the correct type
                try: 
                    #org entry 
                    #"xywh=pixel:538.0361938476562,231.3452911376953,167.65130615234375,156.95021057128906"
                    boundingBoxStr = seg["rawData"][key].target["selector"]["value"].split(':')[1]
                    xywhAsString = boundingBoxStr.split(",")
                    polygonPoints = Dataset.convertXYWHStringsTo4points(xywhAsString)                  
                    polygonPoints = transformPolygonPoints(np.array(polygonPoints),trafoMatrix)
            
                except Exception as e:
                    print(seg["rawData"][key].target["selector"])
                    print(e)
                    continue
            #only save the transformed annotations as polygons
            newPointsString = '<svg><polygon points=\"'
            for i, point in enumerate(polygonPoints):
                if i < len(polygonPoints)-1:
                    pStr = f"{point[0]},{point[1]} "
                else:
                    #last point without space in string 
                    pStr = f"{point[0]},{point[1]}"
                newPointsString = newPointsString + pStr
            newPointsString = f'{newPointsString}\" /></svg>'
            #overwrite the old annotations 
            seg["rawData"][key].target["selector"]["type"] = "SvgSelector"
            seg["rawData"][key].target["selector"]["value"] = newPointsString
            newJson = seg["rawData"].to_json()
            segId  = seg["id"]
            resultFile = f"{resultPath}/{segId}.png.json"
            with open(resultFile, 'w') as file:
                file.write(newJson)