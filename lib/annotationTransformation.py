"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import numpy as np

def transformPolygonPoints(points,trafoMatrix):
    newPoints = []
    for point in points:
        point = np.append(point,[1],axis=0)
        newPoint = np.round(np.matmul(trafoMatrix, point.T))
        newPoint[0] = int(round(newPoint[0]/newPoint[2]))
        newPoint[1] = int(round(newPoint[1]/newPoint[2]))
        newPoints.append(newPoint[:2])
    return newPoints

def transformAnnotations(annotations,trafoMatrixRaw):
    
    trafoMatrix = np.concatenate((np.array(trafoMatrixRaw),np.array([[0,0,1]])),axis=0)
    #for the annotation we have to use back projection
    trafoMatrix = np.linalg.inv(trafoMatrix)
    transformedAnnotations = []
    for polygon in annotations:
        transformedPolygon = transformPolygonPoints(polygon[0],trafoMatrix)
        transformedAnnotations.append(np.array([transformedPolygon]).astype(int))

    return transformedAnnotations