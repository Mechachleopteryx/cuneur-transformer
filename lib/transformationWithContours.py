"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import cv2
import numpy as np


class TransformationWithContours():

    def __init__(self, img, targetResolution):
        self.img = img
        self.targetResolution = targetResolution
        self.transformedImage = img
    
    def cropAndTransformTheBiggestContour(self):
        gray=cv2.cvtColor(self.img,cv2.COLOR_BGR2GRAY)
        #get the contours, the threshold is the border to the black background
        #usually the background of fatcrosses and other photographed tablets are black/dark
        ret, thresh = cv2.threshold(gray, 30, 255, cv2.THRESH_BINARY)

        #search for the biggest bounding box of a contour inside the binary image
        contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)
        bboxes = []
        for cnt in contours:
            x,y,w,h = cv2.boundingRect(cnt)
            size = w*h
            bboxes.append([size,x,y,w,h])
        #sort by the first column (size)
        bboxesNp = np.array(bboxes)
        sortedBBoxes = bboxesNp[np.argsort(bboxesNp[:,0])]
        
        #largest bbox
        _, x, y, w, h = sortedBBoxes[-1]

        #cut the image
        #-1 because we can get a crop from fatcross extracted
        #in this case the largest bounding box is defined with the image borders --> access by index error
        imgCrop = self.img[y:y+h-1,x:x+w-1]
        self.transformedImage = cv2.resize(imgCrop,self.targetResolution)
    
    def getIoUwithCuneurSegmentContour(self,cuneurImg):
        transformedCropGray = cv2.cvtColor(self.transformedImage,cv2.COLOR_BGR2GRAY)
        renderedCropGray = cv2.cvtColor(cuneurImg,cv2.COLOR_BGR2GRAY)
        #threshold for the black background
        ret, threshTransformed = cv2.threshold(transformedCropGray, 30, 80, cv2.THRESH_BINARY)
        #threshold for the white background
        ret, threshRendered = cv2.threshold(renderedCropGray, 240, 250, cv2.THRESH_BINARY_INV)

        transformedContours, hierarchy = cv2.findContours(threshTransformed,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
        largestTransformedContour = TransformationWithContours.getLargestContour(transformedContours)
        renderedContours, hierarchy = cv2.findContours(threshRendered,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_NONE)
        largestRenderedContour = TransformationWithContours.getLargestContour(renderedContours)

        cv2.drawContours(threshRendered,[largestRenderedContour],-1,255,cv2.FILLED)
        cv2.drawContours(threshTransformed,[largestTransformedContour],-1,255,cv2.FILLED)

        #inside the contours are all pixels 255
        #the idea is that the diffence between the size of the clay tablet contour could be a hint of a transformation error
        #because sometimes the renderings have a bigger area and the transformation with scaling is not working

        #calculate IoU on pixel level
        #resize the images to improve the performance
        resizedTRendered = cv2.resize(threshRendered,(round(0.5*threshRendered.shape[1]),round(0.5*threshRendered.shape[0])))
        resizedTTransformed = cv2.resize(threshTransformed,(round(0.5*threshRendered.shape[1]),round(0.5*threshRendered.shape[0])))

        intersectedPixel = 0
        unionPixel = 0 
        for renderedRow, transformedRow in zip(resizedTRendered,resizedTTransformed):
            for renderedVal, transformedVal in zip(renderedRow,transformedRow):
                if renderedVal == 255 and transformedVal == 255:
                    intersectedPixel += 1
                    unionPixel +=1
                    continue
                if renderedVal == 255 or transformedVal == 255:
                    unionPixel +=1
                
        IoU = intersectedPixel/unionPixel
        return IoU
    
    def getLargestContour(contours):
        LargestContour = contours[0]
        for cnt in contours:
            if len(LargestContour) < len(cnt):
                LargestContour = cnt
        return LargestContour    