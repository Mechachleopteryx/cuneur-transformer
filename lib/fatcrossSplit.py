"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import cv2
import numpy as np

class FatcrossSplit():
    def __init__(self,imgPath):
        self.fatcrossImg = cv2.imread(imgPath)
        self.backCrop = None
        self.frontCrop = None
        self.splitFatcross()

    #uses the 2 biggest Bounding Boxes (defined by the largest contours)
    def splitFatcross(self):
        gray=cv2.cvtColor(self.fatcrossImg,cv2.COLOR_BGR2GRAY)
        #get the contours, the threshold is the border to the black background
        #usually the background of fatcrosses and other photographed tablets are black/dark
        ret, thresh = cv2.threshold(gray, 30, 255, cv2.THRESH_BINARY)
        contours, hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)

        #search the two biggest bounding boxes -> countour of the front and the back segment
        bboxes = []
        for cnt in contours:
            x,y,w,h = cv2.boundingRect(cnt)
            size = w*h
            bboxes.append([size,x,y,w,h])
        #sort by the first column
        bboxesNp = np.array(bboxes)
        sortedBBoxes = bboxesNp[np.argsort(bboxesNp[:,0])]
    
        _, x1, y1, w1, h1 = sortedBBoxes[-1]
        _, x2, y2, w2, h2 = sortedBBoxes[-2]        

        #the front segment is always at a higher position in the fatcross
        if y1 > y2:
            self.frontCrop = self.fatcrossImg[y1:y1+h1,x1:x1+w1]
            self.backCrop = self.fatcrossImg[y2:y2+h2,x2:x2+w2]
        else:
            self.backCrop = self.fatcrossImg[y1:y1+h1,x1:x1+w1]
            self.frontCrop = self.fatcrossImg[y2:y2+h2,x2:x2+w2]

    def getCrop(self,side="front",switched = False):
        crop = None
        if side == "front":
            if switched:
                crop = self.frontCrop
            else:
                crop = self.backCrop
        if side == "back":
            if switched:
                crop = self.backCrop
            else:
                crop = self.frontCrop
        return crop 