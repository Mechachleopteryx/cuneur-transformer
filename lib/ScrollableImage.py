"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import cv2
import tkinter as tk
from tkinter import messagebox
from PIL import Image, ImageTk
import numpy as np
import copy 
import platform 

KEYPOINT_COLOR = '#02E415'
KEYPOINT_LABELING_COLOR = "#0509FE"



#source https://stackoverflow.com/questions/56043767/show-large-image-using-scrollbar-in-python
class ScrollableImage(tk.Frame):
    def __init__(self, master=None, **kw):
        self.image = kw.pop('image', None)
        self.transformedImage = None
        sw = kw.pop('scrollbarwidth', 10)
        super(ScrollableImage, self).__init__(master=master, **kw)
        self.cnvs = tk.Canvas(self, highlightthickness=0, **kw)
        self.cnvs.create_image(0, 0, anchor='nw', image=self.image)
        self.cnvs.bind("<Button-1>", self.left_mouse_click)
        self.cnvs.bind("<Button-3>", self.right_mouse_click)
        # Vertical and Horizontal scrollbars
        self.v_scroll = tk.Scrollbar(self, orient='vertical', width=sw)
        self.h_scroll = tk.Scrollbar(self, orient='horizontal', width=sw)
        # Grid and configure weight.
        self.cnvs.grid(row=0, column=0,  sticky='nsew')
        self.h_scroll.grid(row=1, column=0, sticky='nsew')
        self.v_scroll.grid(row=0, column=1, sticky='nsew')
        self.rowconfigure(0, weight=50)
        self.rowconfigure(1, weight=1)
        self.columnconfigure(0, weight=50)
        self.columnconfigure(1, weight=1)
        # Set the scrollbars to the canvas
        self.cnvs.config(xscrollcommand=self.h_scroll.set, 
                           yscrollcommand=self.v_scroll.set)
        # Set canvas view to the scrollbars
        self.v_scroll.config(command=self.cnvs.yview)
        self.h_scroll.config(command=self.cnvs.xview)
        # Assign the region to be scrolled 
        self.cnvs.config(scrollregion=self.cnvs.bbox('all'))
        if platform.system() == "Windows":
            self.cnvs.bind_class(self.cnvs, "<MouseWheel>", self.mouse_scroll)
        else:
            self.cnvs.bind_class(self.cnvs, "<Button-4>", self.mouse_scroll_up)
            self.cnvs.bind_class(self.cnvs, "<Button-5>", self.mouse_scroll_down)

        self.selectedKeypoints = []
        #canvas elements are necessary to undo the last step
        self.ovalsStack = []
        self.keypointLabelsStack = []


    #Scroll Linux
    #------------------------------
    def mouse_scroll_up(self, evt):
        #only scroll canvas image if it's necessary
        if self.image.height() > self.winfo_height():
            self.cnvs.yview_scroll(-1, 'units')


    def mouse_scroll_down(self, evt):
        if self.image.height() > self.winfo_height():
            self.cnvs.yview_scroll(1, 'units') 
    #-----------------------------
    #Scroll Windows
    def mouse_scroll(self,evt):
        if evt.state == 0 :
            self.cnvs.yview_scroll(int(-1*(evt.delta/120)), 'units') 
        if evt.state == 1:
            self.cnvs.xview_scroll(int(-1*(evt.delta/120)), 'units') 

    #draw Points around the selected Keypoints and save them
    def left_mouse_click(self,event):
        #scrollbar offset
        scrollValueY = round(self.v_scroll.get()[0]*self.image.height())
        scrollValueX = round(self.h_scroll.get()[0]*self.image.width())
        x1, y1 = (event.x +scrollValueX - 3), (event.y + scrollValueY - 3)
        x2, y2 = (event.x +scrollValueX + 3), (event.y+ scrollValueY + 3)

        self.ovalsStack.append(self.cnvs.create_oval(x1, y1, x2, y2, fill=KEYPOINT_COLOR))
        self.keypointLabelsStack.append(self.cnvs.create_text(event.x +scrollValueX+1, event.y + scrollValueY +12, font=("Purisa", 12), text=str(len(self.selectedKeypoints)+1),fill=KEYPOINT_LABELING_COLOR))
        self.selectedKeypoints.append([event.x +scrollValueX,event.y + scrollValueY ])
    #Undo last step
    def right_mouse_click(self,event):
        self.cnvs.delete(self.ovalsStack[-1])
        self.cnvs.delete(self.keypointLabelsStack[-1])
        del self.ovalsStack[-1]
        del self.keypointLabelsStack[-1]
        del self.selectedKeypoints[-1]
        

    def resetCanvasImage(self):
        self.cnvs.delete("all")
        self.cnvs.create_image(0, 0, anchor='nw', image=self.image)
        self.selectedKeypoints = []
        self.ovalsStack = []
        self.keypointLabelsStack = []

    def setNewImage(self,newImg):
        self.cnvs.delete("all")
        self.image = newImg
        self.cnvs.create_image(0, 0, anchor='nw', image=self.image)
        self.ovalsStack = []
        self.keypointLabelsStack = []
        self.drawCurrentMarkers()

    def drawCurrentMarkers(self):
        for i, point in enumerate(self.selectedKeypoints):
            x1, y1 = (point[0] - 3), (point[1] - 3)
            x2, y2 = (point[0] + 3), (point[1] + 3)
            self.ovalsStack.append(self.cnvs.create_oval(x1, y1, x2, y2, fill=KEYPOINT_COLOR))
            self.keypointLabelsStack.append(self.cnvs.create_text(point[0]+1, point[1] +12, font=("Purisa", 12), text=str(i+1),fill=KEYPOINT_LABELING_COLOR))
        
