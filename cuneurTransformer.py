"""
    Cuneur Transformer- Transform annotations of the Cuneur between different images e.g. from 3D rendering to photograph.
    Copyright (C) 2023  Ernst Stoetzner 

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import cv2
import os
import numpy as np
from tkinter import messagebox
from lib.dataset  import Dataset
from lib.ScrollableImage import  ScrollableImage
from lib.fatcrossSplit import FatcrossSplit
from lib.transformationWithContours import TransformationWithContours
from lib.annotationTransformation import *
import copy
import tkinter as tk
from tkinter import messagebox, filedialog
from PIL import Image, ImageTk
from tkinter.ttk import Label,Checkbutton, Scale
import time
import platform 

FILE_SUFFIXES = ['.jpg','.png']
FILEDIALOG_FILE_TYPES = [("jpg",".jpg"),("png",".png")]

IMAGE_WINDOW_WIDTH = 300
IMAGE_WINDOW_HEIGHT = 500

class CuneurTransformer():
    def __init__(self):
        ##########################################
        #GUI Definition 
        ##########################################
        self.root = tk.Tk()
        self.root.title("Cuneur Transformer")
        #file dialogs withouut theme because with the theme the directories are not readable
        self.dialogTk = tk.Tk()
        self.dialogTk.withdraw()
        #set theme from 
        #https://github.com/rdbende/Azure-ttk-theme
        # Set the initial theme
        self.root.tk.call("source", "azure.tcl")
        self.root.tk.call("set_theme", "dark")
        #add icon taskbar
        self.root.iconphoto(True, tk.PhotoImage(file='./cuneiform_annotator_transformer_logo_200px.png'))
    

        #full screen
        if platform.system() == "Windows":
            self.root.state("zoomed")
        else:
            self.root.wm_attributes("-zoomed", True)
        # configure the grid
        self.root.columnconfigure(0, weight=1)
        self.root.columnconfigure(1, weight=5)
        self.root.columnconfigure(2, weight=5)
        self.root.rowconfigure(3, weight=5)
     
        #self.inputImgtk = ImageTk.PhotoImage(image=None)       
        #targetImgtk = ImageTk.PhotoImage(image=None) 

        #start button
        self.startButton = tk.Button(self.root, text="Start", command=self.startTransformationsProcess,state='disabled')
        self.startButton.grid(row=0,column=0,padx=3,pady=3,sticky=tk.NSEW)
        
        #Fatcross Modus
        self.fatcrossModusActivated = tk.BooleanVar()
        self.fatcrossModusActivated.set(True)
        self.switchFatcrossModus = Checkbutton(self.root, text='Fatcrosses as input', style='Switch.TCheckbutton', variable=self.fatcrossModusActivated)
        self.switchFatcrossModus.grid(row=0,column=1,padx=5,pady=3, sticky=tk.NW)

        #image transformation modus
        #image transformation modus - images are transformed so that they are congruent with the cuneur images
        # annotation transformation - annotations are transformed so that they lay at the correct position in the input image 
        self.imageTransformationModusActivated = tk.BooleanVar()
        self.imageTransformationModusActivated.set(True)
        self.switchTransformationModus = Checkbutton(self.root, text='Transform input images, otherwise: annotations', style='Switch.TCheckbutton', variable=self.imageTransformationModusActivated)
        self.switchTransformationModus.grid(row=1,column=1,padx=5,pady=3, sticky=tk.NW)

        #skip Modus 
        self.skipExistingTrans = tk.BooleanVar()
        self.switchSkipModus = Checkbutton(self.root, text='Skip segment if a transformation of the segment already exists', style='Switch.TCheckbutton', variable=self.skipExistingTrans)
        self.switchSkipModus.grid(row=2,column=1,padx=5,pady=3, sticky=tk.NW)

        #IoU Threshold with label
        self.IoUThreshold = tk.IntVar()
        self.IoUThreshold.set(99)
        self.IoUThresholdLabel = Label(self.root, text=f"Skip segments with IoU of {self.IoUThreshold.get()}%")
        self.IoUThresholdLabel.grid(row=1,column=0,padx=5)
        self.IoUScale = Scale(self.root, style='Tick.TScale', from_=0,to=100, variable=self.IoUThreshold, command=self.updateIoUThreshold)
        self.IoUScale.grid(row=2,column=0,padx=5,sticky=tk.NSEW)
        #label of the calculated IoU 
        self.IoU = tk.DoubleVar()
        self.IoU.set(0.0)
        

        #label
        #shows loading state and number of transformed image during the transformation process
        self.annotationText = Label(self.root, text="Loading...")
        self.annotationText.grid(row=0,column=2,sticky=tk.NSEW)



        ########################################
        
        #variables of the switch buttons to toggle the annotation visualization 
        self.showAnnotationsOnTransformedImg = tk.BooleanVar()
        self.showAnnotationsOnTransformedImg.set(True)
        self.showAnnotationsOnTransformedImg.trace("w",self.showAnnotationsChanged)
        self.showAnnotationsOnCuneurImg = tk.BooleanVar()
        self.showAnnotationsOnCuneurImg.set(True)
        self.showAnnotationsOnCuneurImg.trace("w",self.showAnnotationsChanged)
        #variables of the switch button to switch front and back 
        self.isFatcrossSegSwitched = tk.BooleanVar()
        self.isFatcrossSegSwitched.set(False)
        self.isFatcrossSegSwitched.trace("w",self.switchFrontAndBack)

        #index of the segment/annotation iteration 
        self.currentSegIdx = 0
    
        annotationsDirectory = ''
        self.cuneurImagesDir = ''
        self.inputImagesDir = ''
        self.resultsDir = ''
        
        while not annotationsDirectory:
            annotationsDirectory = filedialog.askdirectory(parent=self.dialogTk, title="ANNOTATIONS: Select the directory where the annotations are located!")
        #use sleep because the user should notice that he selected the directory successfully
        time.sleep(0.5)
        #directory with the images used inside the cuneur annotator
        while not self.cuneurImagesDir:
            self.cuneurImagesDir = filedialog.askdirectory(parent=self.dialogTk,title="CUNEUR IMAGES: Select the directory where the Cuneur images are located!")
        time.sleep(0.5)
        #directory with the images to be transformed
        while not self.inputImagesDir:
            self.inputImagesDir = filedialog.askdirectory(parent=self.dialogTk,title="INPUT IMAGES: Select the directory where the images for transformation are located!")
        time.sleep(0.5)
        #directory to save the transformed images or annotations
        while not self.resultsDir: 
            self.resultsDir = filedialog.askdirectory(parent=self.dialogTk,title="TARGET DIRECTORY: Select the directory where the results are saved!")
        #the tkinter window config is not needed longer
        self.dialogTk.destroy()

        Dataset.createDirIfNotExists(self.resultsDir)
        self.annotationsDataset = Dataset(annotationsDirectory)
        #self.annotationsDataset.deleteSegmentWithoutImage(self.cuneurImagesDir)

        #ready for transformation!
        self.startButton["state"] = 'active'
        nrOfLoadedAnnotations = len(self.annotationsDataset.tabletSegments)
        self.annotationText["text"] = f'{nrOfLoadedAnnotations} annotations were loaded successfully!' 
        self.root.mainloop()


    def executeTransformation(self):
        if len(self.inputImage_window.selectedKeypoints) != len(self.targetImage_window.selectedKeypoints):
            messagebox.showerror("Keypoint Error","The number of selected Keypoints inside the Input image should be the same as in the target image!")
            return False
        
        if len(self.inputImage_window.selectedKeypoints) < 3:
            messagebox.showerror("Keypoint Error","Select at least 3 Keypoints!")
            return False

        inputKeypoints = np.array(self.inputImage_window.selectedKeypoints).astype(np.float32)
        targetKeypoints = np.array(self.targetImage_window.selectedKeypoints).astype(np.float32)
        #second return value are the inliers
        #transformationMatrix,_ = cv2.estimateAffinePartial2D(inputKeypoints,targetKeypoints)
        self.transformationMatrix,_ = cv2.estimateAffine2D(inputKeypoints,targetKeypoints, maxIters=5000)
        
        if self.imageTransformationModusActivated.get():
            self.transformedImage = cv2.warpAffine(self.transformedImage, self.transformationMatrix, (self.currentCuneurImg.shape[1],self.currentCuneurImg.shape[0]))
        else:
            self.currentSegTransAnnotations = transformAnnotations(self.currentSegAnnotations, self.transformationMatrix) 

        self.drawAnnotationsOnTransformedImage()
        transformedImgTK = self.getTransformedImageAsTK() 

        self.inputImage_window = ScrollableImage(self.root, image=transformedImgTK , scrollbarwidth=6, 
                               width=IMAGE_WINDOW_WIDTH, height=IMAGE_WINDOW_HEIGHT)
        self.inputImage_window.grid(row=3,column=1,padx=5, pady=5, sticky=tk.NSEW)
        #reset the annotations button. You always want to see annotations after transformation 
        self.showAnnotationsOnTransformedImg.set(True)


    def acceptTransformation(self):
        if self.imageTransformationModusActivated.get():
            #save transformed image
            cv2.imwrite(f'{self.resultsDir}/{self.currentSegId}.jpg', self.transformedImage)
        else:
            #transform the annotations and save them in a new json 
            self.annotationsDataset.transfAndSaveSegJsonByIdx(self.currentSegIdx,self.transformationMatrix,self.resultsDir)


        #next annotation
        self.currentSegIdx += 1

        if self.fatcrossModusActivated.get():
            #reset the switch button
            if self.isFatcrossSegSwitched.get():
                #the new transformation will be triggered automatically after the set method
                self.isFatcrossSegSwitched.set(False) 
            else:
                self.createTransformationOfCurrentAnnotation()
        else:
            self.createTransformationOfCurrentAnnotation()

    def resetTransformation(self):
        #reset to the initial transformation 
        self.transformedImage = self.transformationWithContours.transformedImage
        #reset to initial annotations
        self.currentSegTransAnnotations = self.currentSegAnnotations
        self.drawAnnotationsOnTransformedImage()
        transformedImgTK = self.getTransformedImageAsTK()

        self.inputImage_window = ScrollableImage(self.root, image=transformedImgTK , scrollbarwidth=6, 
                               width=IMAGE_WINDOW_WIDTH, height=IMAGE_WINDOW_HEIGHT)
        self.inputImage_window.grid(row=3,column=1,padx=5, pady=5, sticky=tk.NSEW)
        self.targetImage_window.resetCanvasImage()

    def startTransformationsProcess(self):
        #delete start gui
        self.startButton.destroy()
        self.switchFatcrossModus.destroy()
        self.switchSkipModus.destroy()
        self.switchTransformationModus.destroy()

        #start the first transformation and creates the gui first time
        self.createTransformationOfCurrentAnnotation()

    def showAnnotationsChanged(self,var,op1,op2):
        transformedImgTK = self.getTransformedImageAsTK() 
        cuneurImgTK = self.getCuneurImageAsTK() 
        self.inputImage_window.setNewImage(transformedImgTK)
        self.targetImage_window.setNewImage(cuneurImgTK)


    def updateIoUThreshold(self,value):
        self.IoUThresholdLabel["text"] = f"Skip segments with IoU of {self.IoUThreshold.get()}%"

    def switchFrontAndBack(self,var,op1,op2):

        isSwitched = self.isFatcrossSegSwitched.get()
        self.createTransformationOfCurrentAnnotation(isSwitched)
        
    ########################################################
    #functions without button connection 
    ########################################################


    def showManualTransformationGui(self):
        transformedImgTK = self.getTransformedImageAsTK() 
        cuneurImgTK = self.getCuneurImageAsTK() 

        #image windows
        self.inputImage_window = ScrollableImage(self.root, image=transformedImgTK , scrollbarwidth=6, 
                               width=IMAGE_WINDOW_WIDTH, height=IMAGE_WINDOW_HEIGHT)
        self.inputImage_window.grid(row=3,column=1,padx=5, pady=5, sticky=tk.NSEW)
        self.targetImage_window = ScrollableImage(self.root, image=cuneurImgTK, scrollbarwidth=6, 
                               width=IMAGE_WINDOW_WIDTH, height=IMAGE_WINDOW_HEIGHT)
        self.targetImage_window.grid(row=3,column=2,padx=5, pady=5, sticky=tk.NSEW)

        #IoU label
        self.calculatedIoULabel = Label(self.root, text=f"Intersection over Union (IoU) of both segments: {round(self.IoU.get()*100,3)}%")
        self.calculatedIoULabel.grid(row=1,column=1,padx=5, pady=7,sticky=tk.NE )

        #buttons
        transformButton = tk.Button(self.root, text="Transform Image", command=self.executeTransformation)
        transformButton.grid(row=0,column=0,padx=3,pady=3,sticky=tk.NSEW)
        resetButton = tk.Button(self.root, text="Reset ", command=self.resetTransformation)
        resetButton.grid(row=3,column=0,padx=3,pady=5, sticky=tk.NW)
        acceptButton = tk.Button(self.root, text="Accept and save transformed image", command=self.acceptTransformation)
        acceptButton.grid(row=0,column=1,padx=5,pady=3, sticky=tk.NW)
        switchAnnotationsInputImg = Checkbutton(self.root, text='Show annotations', style='Switch.TCheckbutton', variable=self.showAnnotationsOnTransformedImg)
        switchAnnotationsInputImg.grid(row=1,column=1,padx=5, pady=3,sticky=tk.NW )
        switchAnnotationsCuneurImg = Checkbutton(self.root, text='Show annotations', style='Switch.TCheckbutton', variable=self.showAnnotationsOnCuneurImg)
        switchAnnotationsCuneurImg.grid(row=1,column=2,pady=3,sticky=tk.NW)

       
        #switch front and back in case that the fatcross is switched
        if self.fatcrossModusActivated.get():
            switchFrontAndBack = Checkbutton(self.root, text='Switch back and front of the fatcross', style='Switch.TCheckbutton', variable=self.isFatcrossSegSwitched)
            switchFrontAndBack.grid(row=2,column=1,padx=5, pady=3,sticky=tk.NW )

        
    #segment depends on the current index 
    #the function is searching for an image with an to the fitting name 
    #if the fatcross modus is activated then find the corresponding crop
    #calculate the transformation by contours
    #if the the IoU between the transformed segment and the input segment less than the Threshold or the segment contains wedge annotations then show the image in the gui 
    #show annotations on both images
    #else save the transformation and continue with the next annotation file
    def createTransformationOfCurrentAnnotation(self,switchFatcrossSides=False):
        #check if the transformation of the whole dataset is finished
        if self.currentSegIdx >= len(self.annotationsDataset.tabletSegments):
            self.showFinished()
            return
        seg = self.annotationsDataset.tabletSegments[self.currentSegIdx]
        self.currentSegId = seg["id"]
        self.currentSegAnnotations = seg["characters"] + seg["wedges"]
        self.currentSegTransAnnotations = copy.deepcopy(self.currentSegAnnotations)
        self.updateAnnotationLabel()

        if self.skipExistingTrans.get():
            #check if the transformation already exists
            if self.isAlreadyTransformed():
                #skip
                self.currentSegIdx += 1
                self.createTransformationOfCurrentAnnotation() 
                return
        

        self.currentCuneurImg =  self.getCurrentCuneurImg()
        imgPath = self.getImagePath()
        if not imgPath:
            #img for segment id was not found 
            retVal = messagebox.askyesno("Image was not found", f"The image to be transformed for the segment {self.currentSegId} was not found. \nDo you want to specify the path manually? \n \nYes-Specify path \nNo-Skip segment")
            if retVal:
                #create environment with standard theme
                self.dialogTk = tk.Tk()
                self.dialogTk.withdraw()
                imgPath=filedialog.askopenfilename(parent=self.dialogTk,title=f"Select image for segment {self.currentSegId}", initialdir=self.inputImagesDir, filetypes=FILEDIALOG_FILE_TYPES)
                self.dialogTk.destroy()
                
        #if still not a image path is specified the user pressed cancel or skip
        if not imgPath:
            #skip
            self.currentSegIdx += 1
            self.createTransformationOfCurrentAnnotation()
            return False
        segImg = None
        if self.fatcrossModusActivated.get():
            fatcross = FatcrossSplit(imgPath)
            if 'back' in self.currentSegId:
                segImg = fatcross.getCrop("back",switchFatcrossSides)
            if 'front' in self.currentSegId:
                segImg = fatcross.getCrop("front",switchFatcrossSides)
        else:
            segImg = cv2.imread(imgPath)

        #as attribute is necessary for reset functions during the manual transformation
        self.transformationWithContours = TransformationWithContours(segImg,(self.currentCuneurImg.shape[1],self.currentCuneurImg.shape[0]))
        #only do the transformation if the whole image will be transformed
        #a transformation of the input image leads to inaccuracies in the results of the annotations transformation 
        if self.imageTransformationModusActivated.get():
            self.transformationWithContours.cropAndTransformTheBiggestContour()
        self.transformedImage = self.transformationWithContours.transformedImage
        #check the area deviation of the transformed Crop and the original rendered crop
        #area is the claytablet extracted from the background
        #Evaluation measure is the intersection of union
        self.IoU.set(self.transformationWithContours.getIoUwithCuneurSegmentContour(self.currentCuneurImg))
  
        if self.IoU.get() > (self.IoUThreshold.get()/100) and not seg["containsWedgeAnnotations"] and self.imageTransformationModusActivated.get():
            self.acceptTransformation()
        else:
            self.drawAnnotationsOnCuneurImage()
            self.drawAnnotationsOnTransformedImage()
            self.showManualTransformationGui()

            
    #returns the openCV image of the current annotation/segment 
    def getCurrentCuneurImg(self):
        imgPath = os.path.join(self.cuneurImagesDir,f'{self.currentSegId}.jpg')
        return cv2.imread(imgPath)

    #returns the path to the input image corresponding with the annotation 
    def getImagePath(self):
        for filename in os.listdir(self.inputImagesDir):
            for fileSuffix in FILE_SUFFIXES:
          
                if fileSuffix in filename:
                    tabletName = filename.split(fileSuffix)[0]
                else:
                    continue

                if tabletName in self.currentSegId:
                    imgFile = os.path.join(self.inputImagesDir,filename)
                    return imgFile
        return False

    def drawAnnotationsOnCuneurImage(self):
        self.cuneurImgWithPolygons = copy.deepcopy(self.currentCuneurImg) 
        for polygon in self.currentSegAnnotations:
            cv2.polylines(self.cuneurImgWithPolygons, [polygon], True, (255,0,0), thickness=1)
    
    def drawAnnotationsOnTransformedImage(self):
        self.transformedImgWithPolygons = copy.deepcopy(self.transformedImage) 
        for polygon in self.currentSegTransAnnotations:
            cv2.polylines(self.transformedImgWithPolygons, [polygon], True, (255,0,0), thickness=1)
        
    def updateAnnotationLabel(self):
        nrOfAnnotations = len(self.annotationsDataset.tabletSegments)
        newText = f'Current segment: {self.currentSegId} [{self.currentSegIdx+1}/{nrOfAnnotations}]'
        self.annotationText["text"] = newText
        self.root.update()

    def showFinished(self):
        self.annotationText["text"] = "All annotations of the Dataset are transformed!"
        self.root.update()
        messagebox.showinfo("Finished","You transformed all annotations from the directory!")
        self.root.destroy()

    def getTransformedImageAsTK(self):
        if self.showAnnotationsOnTransformedImg.get():
            b,g,r = cv2.split(self.transformedImgWithPolygons)
        else:
            b,g,r = cv2.split(self.transformedImage)
        transformedImg = cv2.merge((r,g,b))
        # Convert the Image object into a TkPhoto object
        transformedImgPIL = Image.fromarray(transformedImg)
        transformedImgTK = ImageTk.PhotoImage(image=transformedImgPIL)
        return transformedImgTK
    def getCuneurImageAsTK(self):
        if self.showAnnotationsOnCuneurImg.get():
            b,g,r = cv2.split(self.cuneurImgWithPolygons)
        else:
            b,g,r = cv2.split(self.currentCuneurImg)
        cuneurImg = cv2.merge((r,g,b))
        cuneurImgPIL = Image.fromarray(cuneurImg)
        cuneurImgTK = ImageTk.PhotoImage(image=cuneurImgPIL)
        return cuneurImgTK

    def isAlreadyTransformed(self):
        for file in os.listdir(self.resultsDir):
            if self.currentSegId in file:
                return True
        return False

if __name__ == "__main__":
    gui = CuneurTransformer()