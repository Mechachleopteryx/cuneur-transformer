# Cuneur Transformer
[![pipeline status](https://gitlab.com/fcgl/cuneur-transformer/badges/main/pipeline.svg)](https://gitlab.com/fcgl/cuneur-transformer/-/commits/main)

## Description
The Cuneur Transformer is a Python-based tool developed specifically for the use in connection with the Cuneiform Annotator [Cuneur](https://gitlab.com/fcgl/annotator-showcase).
Before you are able to use the Cuneur Transformer, you have to create annotations with the Cuneur Annotator. 

This tool offers the possibility to transform these annotations between different images e.g. from 3D rendering to photograph or to transform the images to fit the annotations.
You can use fatcrosses and images of a single segment as input image.

## Installation
You can download the tool directly from the GitLab Artifacts.
It is available for: 
- [Ubuntu](https://gitlab.com/fcgl/cuneur-transformer/-/jobs/artifacts/main/download?job=ubuntu)
- [Windows](https://gitlab.com/fcgl/cuneur-transformer/-/jobs/artifacts/main/download?job=windows)

## Recommended Dataset Preparation
First you need the JSON annoation files of the Cuneur Annotator and the corresponding images. It is recommended to save them in two different directories. 
Furthermore, you have to prepare a second directory that contains all images which you want to use for the transformation.

In case that you are working with fatcross images as input, it is recommended to name them with the ID used by the Cuneur Anotator. Otherwise, the tool will ask you for the path for each image.
To work fluently with photographs of a single segment, it is recommended to name them with the same name that used by the Cuneur Annotator image file e. g. HS_863_03_front. 


## Usage
After the start of the application, you will be asked to specify all directories: 
1. Annotations directory: Must contain the JSON annotations files of the Cuneur Annotator.
2. Cuneur images directory: Must contain the images used by the Cuneur Annotator.
3. Input images directory: Must contain the images which you want to use for the transformation.
4. Target directory: Specify the path where the transformation result should be saved.

In the following start screen, you can decide whether you want use fatcrosses or single segments as input. Also you have to choose whether you want to map the image to fit the annotations or if you want to transform the annotations themselves.

With the Intersection over Union (IoU) threshold, you define when a transformation is skipped automatically (only for image transformations). If the IoU of the area of automatic transformed segment with the area of the Cuneur Annotator segment is higher than the threshold, then the manual alignment will not be started.

Press "Start" to start the transformation process. 

In the transformation screen, all you have to do is mark corresonding keypoints in both images with the left mouse button (at least 3). After that press "Transform" and the result will be shown. To save the result, press "Accept Transformation" and the application continues with the next segment until the entire dataset is processed.

## Authors
Developed at the Martin Luther University of Halle-Wittenberg.
- [Ernst Stötzner](https://orcid.org/my-orcid?orcid=0009-0003-6716-2979) initial author and developer
- [Hubert Mara](https://orcid.org/0000-0002-2004-4153) contributor
- [Timo Homburg](https://orcid.org/0000-0002-9499-5840) contributor and author of the [Cuneur Annotator](https://gitlab.com/fcgl/annotator-showcase)

